class Dal:

    def __init__(self, db):
        self.db = db
        self.q = db.AQLQuery

    def get_by_slug(self, slug: str):
        try:
            return self.q(
                '''
                    FOR m IN mangas FILTER m.slug == @slug LIMIT 1 RETURN m._id
                ''',
                rawResults=True, bindVars={'slug': slug}
            )[0]
        except:
            return None

    def insert_manga(self, info: dict):
        return self.q(
            '''
                INSERT {
                    title: @title,
                    slug: @slug,
                    url: @url,
                    capa: @capa,
                    status: @status,
                    authors: @authors,
                    genres: @genres,
                    summary: @summary,
                    search: @search,
                    created: @created,
                    updated: @updated
                }
                INTO mangas
                LET inserted = NEW
                RETURN inserted._id
            ''',
            rawResults=True, bindVars=info
        )[0]

    def has_chapter(self, manga: str, number: float):
        return self.q(
            '''
                RETURN
                    LENGTH (
                        FOR c IN chapters
                            FILTER c.manga == @manga 
                            FILTER c.number == @number
                        RETURN true
                    ) > 0
            ''',
            rawResults=True, bindVars={'manga': manga, 'number': number}
        )[0]

    def insert_chapter(self, chapter: dict):
        self.q(
            '''
                INSERT {
                    url: @url,
                    slug: @slug,
                    manga: @manga,
                    number: @number,
                    imgs: @imgs,
                    created: @created
                } INTO chapters
            ''',
            rawResults=True, bindVars=chapter
        )

    def touch_mangas(self, mangas: dict):
        for k in mangas.keys():
            self.q(
                '''
                    FOR m IN mangas
                        FILTER m.slug == @slug
                        UPDATE m WITH { updated: @timestamp }
                    IN mangas
                ''',
                bindVars={ 'slug': k, 'timestamp': mangas[k] }
            )
