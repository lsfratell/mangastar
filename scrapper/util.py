from slugify import slugify

class Util:

    @staticmethod
    def status(d):
        status = d.find(string='Status :').find_next('td').get_text(strip=True)
        return dict(
            slug=slugify(status),
            name=status
        )

    @staticmethod
    def authors(d):
        authors = d.find(string='Author(s) :').find_next('td').select('a')
        return [dict(slug=slugify(a.get_text(strip=True)), name=a.get_text(strip=True)) for a in authors]

    @staticmethod
    def genres(d):
        genres = d.find(string='Genres :').find_next('td').select('a')
        return [dict(slug=slugify(a.get_text(strip=True)), name=a.get_text(strip=True)) for a in genres]

    @staticmethod
    def summary(d):
        return d.find(string='Description :').parent.parent.get_text(strip=True).split('Description :')[1].strip()

    @staticmethod
    def chapters(d):
        return [a.get('href') for a in d.select('.row-content-chapter li a')]

    @staticmethod
    def imgs(d):
        return [img.get('src') for img in d.select('.container-chapter-reader img')]

    @staticmethod
    def capa(d):
        return d.select_one('body > div.body-site > div.container.container-main > div.container-main-left > div.panel-story-info > div.story-info-left > span.info-image > img').get('src')
