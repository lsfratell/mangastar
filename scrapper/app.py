import gevent
import gevent.pool
import gevent.monkey
gevent.monkey.patch_all()

import requests
from dal import Dal
from util import Util
from slugify import slugify
from bs4 import BeautifulSoup
from datetime import datetime, timezone
from pyArango.connection import Connection


class Scrapper:

    def __init__(self):
        self.pool = gevent.pool.Pool(200)
        self.session = requests.Session()
        self.base_url = 'https://manganelo.com/genre-all'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
            'referer': self.base_url
        }
        self.session.headers.update(self.headers)
        self.connection = Connection(username='mangastar', password='123456')
        self.db = self.connection['mangastar']
        self.dal = Dal(self.db)
        self.to_touch = {}

    def get_timestamp(self):
        return int(datetime.now(tz=timezone.utc).timestamp() * 1000)

    def get_bs(self, content: str):
        return BeautifulSoup(content, 'html.parser')

    def now(self):
        return datetime.now()

    def log(self, msg: str):
        print('[{}] - {}'.format(self.now(), msg))

    def get_updates(self):
        urls = [
            "https://manganelo.com/genre-all",
            "https://manganelo.com/genre-all/2"
        ]
        # for i in range(2, 52):
        #     urls.append("https://manganelo.com/genre-all/{0}".format(i))       
        result = []
        for url in urls:
            resp = self.session.get(url)
            if resp.status_code != 200:
                self.log('Couldn\'t get updated mangas from page: {0}'.format(url))
                continue
            document = self.get_bs(resp.text)
            result.extend([
                dict(
                    title=a.get_text(strip=True),
                    url=a.get('href')
                ) for a in document.select('.genres-item-info h3 a')
            ])
        return result

    def check_chapter(self, manga: str, url: str):
        number = float(url.split('chapter_')[1])
        if not self.dal.has_chapter(manga, number):
            self.log('Getting imgs from url: {}'.format(url))
            resp = self.session.get(url)
            if resp.status_code != 200:
                self.log('Couldn\'t fetch chapter page: {}, status: {}'.format(url, resp.status_code))
                return
            document = self.get_bs(resp.text)
            chapter = dict(
                url=url,
                slug=slugify('chapter {}'.format(number)),
                number=number,
                manga=manga,
                imgs=Util.imgs(document),
                created=self.get_timestamp()
            )
            self.to_touch[chapter['manga']] = self.get_timestamp()
            self.dal.insert_chapter(chapter)

    def check_update(self, update: dict):
        self.log('In Queue: {}'.format(update.get('title')))
        title = update.get('title')
        url = update.get('url')
        slug = slugify(title)
        resp = self.session.get(url)
        if resp.status_code != 200:
            self.log('Couldn\'t fetch manga page: {}, status: {}'.format(url, resp.status_code))
            return None
        document = self.get_bs(resp.text)
        manga_id = self.dal.get_by_slug(slug)
        if not manga_id:
            summary = Util.summary(document)
            info = dict(
                title=title,
                slug=slug,
                url=url,
                capa=Util.capa(document),
                status=Util.status(document),
                authors=Util.authors(document),
                genres=Util.genres(document),
                summary=summary,
                search='{} {}'.format(title, summary),
                created=self.get_timestamp(),
                updated=self.get_timestamp()
            )
            manga_id = self.dal.insert_manga(info)
        return slug, Util.chapters(document)

    def start(self):
        results = gevent.joinall([self.pool.spawn(self.check_update, update) for update in self.get_updates()])
        tasks = []
        for result in results:
            if result.value is not None:
                manga, urls = result.value
                for url in urls:
                    tasks.append(self.pool.spawn(self.check_chapter, manga, url))
        gevent.wait(tasks)
        self.dal.touch_mangas(self.to_touch)


if __name__ == "__main__":
    obj = Scrapper()
    start = datetime.now()
    obj.start()
    print("Elapsed time:", datetime.now() - start)