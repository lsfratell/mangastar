const dayjs = require("dayjs");
dayjs.extend(require("dayjs/plugin/relativeTime"));

DEF.helpers.fromNow = function (date) {
  return dayjs(date).fromNow();
};

DEF.helpers.authors = function (authors) {
  return authors.map((a) => a.name).join(", ");
};

DEF.helpers.authorsLink = function (authors) {
  return authors.map((a) => `<a href="/directory?author=${a.slug}" alt=""><span class="label label-secondary">${a.name}</span></a>`).join(" ");
};

DEF.helpers.genresLink = function (genres) {
  return genres.map((a) => `<a href="/directory?genre=${a.slug}" alt=""><span class="label label-primary mb-1">${a.name}</span></a>`).join(" ");
};

DEF.helpers.isActive = function(r, v) {
  return r.menuActive === v ? 'active' : ''
}

DEF.helpers.slugify = function(v) {
  const a = String(v)
  if (a.includes(".")) {
    return a.slug();
  } else {
    return `${a}.0`.slug()
  }
}