var { aql } = require("arangojs");
MAIN.session = SESSION();

MAIN.session.ondata = function (meta, next) {
  FUNC.db()
    .query(
      aql`
        FOR u IN users
          FILTER u._id == ${meta.id}
          LET b = (
            FOR b IN bookmarks
              FILTER b.user_id == u._id
            RETURN b
          )
        RETURN MERGE(u, { bookmarks: b })
      `
    )
    .then((cursor) => cursor.all())
    .then((user) => {
      if (user.length > 0) {
        next(null, user[0]);
      } else {
        next(false);
      }
    });
};

AUTH(function ($) {
  var opt = {};
  opt.name = CONF.auth_cookie;
  opt.key = CONF.auth_secret;
  opt.expire = "3 days";

  MAIN.session.getcookie($, opt, $.done());
});
