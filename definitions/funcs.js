const { aql, Database } = require("arangojs");
const { spawn } = require("child_process");
const { flattenDeep, unionBy, sortBy } = require("lodash");

var db = null;

FUNC.db = function () {
  if (!db) {
    db = new Database({
      url: CONF.db_hostname,
      databaseName: CONF.db_database,
      auth: {
        username: CONF.db_username,
        password: CONF.db_password,
      },
    });
  }
  return db;
};

FUNC.runScrapper = function () {
  const cmd = spawn(CONF.python, [CONF.script], {
    cwd: process.cwd(),
    detached: true,
    stdio: "inherit",
  });

  cmd.once("close", () => FUNC.listAuthorsGenres());
};

FUNC.listAuthorsGenres = function () {
  FUNC.db()
    .query(
      aql`
        LET authors = (FOR m IN mangas RETURN m.authors)
        LET genres = (FOR m IN mangas RETURN m.genres)
        RETURN { authors, genres }
      `
    )
    .then((cursor) => cursor.all())
    .then((data) => {
      DEF.authors = sortBy(unionBy(flattenDeep(data[0].authors), 'slug'), 'name');
      DEF.genres = sortBy(unionBy(flattenDeep(data[0].genres), 'slug'), 'name');
    });
};
