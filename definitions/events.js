ON("service", function (counter) {
  if (counter % 45 === 0)
    FUNC.runScrapper();
});

ON("ready", () => {
  FUNC.listAuthorsGenres();
  if (CONF.on_ready)
    FUNC.runScrapper();
});

ON("controller", function ($) {
  const path = $.req.uri.path;
  const query = $.query;

  if (path === "/") {
    $.repository.menuActive = "home";
  } else if (path === "/directory") {
    $.repository.menuActive = "directory";
  } else if (query.order) {
    $.repository.menuActive = query.order;
  } else if (query.status) {
    $.repository.menuActive = query.status;
  } else if (query.author) {
    $.repository.menuActive = query.author;
  } else {
    $.repository.menuActive = query.genre;
  }
});
