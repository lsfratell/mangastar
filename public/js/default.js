function reverseChapters() {
  var parent = document.getElementById("chapter-list");
  for (var i = 1; i < parent.childNodes.length; i++) {
    parent.insertBefore(parent.childNodes[i], parent.firstChild);
  }
}

function onChapterSelectChange(select) {
  window.location = select[select.selectedIndex].value;
}

function toggle(id) {
  var el = document.getElementById(id);
  ["hide-xs", "hide-sm"].map((css) => el.classList.toggle(css));
}

function toggleModal(id) {
  document.getElementById(id).classList.toggle("active");
}

function formLoginSubmit(evt, form) {
  evt.preventDefault();
  var toast = document.getElementById("modal-login-toast");
  const data = new FormData(form);

  fetch("/login", {
    method: "POST",
    body: data,
  })
    .then((resp) => resp.json())
    .then((resp) => {
      if (resp.success) {
        configureToast(toast, true, "Successful Login");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      } else {
        configureToast(toast, false, "Username/Password incorrect");
      }
    });
}

function formRegisterSubmit(evt, form) {
  evt.preventDefault();
  var toast = document.getElementById("modal-register-toast");
  const data = new FormData(form);

  fetch("/register", {
    method: "POST",
    body: data,
  })
    .then((resp) => resp.json())
    .then((responseData) => {
      if (responseData.success) {
        configureToast(toast, true, "Success, you can login now");
        setTimeout(() => {
          toggleModal("modal-register");
          toggleModal("modal-login");
        }, 1500);
      } else {
        configureToast(toast, false, responseData.value);
      }
    });
}

function configureToast(toast, v, text) {
  if (v) {
    toast.classList.remove("toast-error");
    toast.classList.add("toast-success");
  } else {
    toast.classList.remove("toast-success");
    toast.classList.add("toast-error");
  }

  toast.innerText = text;
  toast.style.display = "block";
}

function bookmarkManga(manga, button) {
  button.classList.toggle("loading");
  fetch("/bookmarks/add/" + manga)
    .then((resp) => resp.json())
    .then((resp) => {
      if (resp.success) {
        ["loading", "btn-primary"].map((x) => button.classList.toggle(x));
      } else {
        ["loading", "btn-error"].map((x) => button.classList.toggle(x));
      }
    }).catch(console.log);
}

function unBookmarkManga(manga, button) {
  button.classList.toggle("loading");
  fetch("/bookmarks/remove/" + manga)
    .then((resp) => resp.json())
    .then((resp) => {
      if (resp.success) {
        ["loading", "btn-primary"].map((x) => button.classList.toggle(x));
      } else {
        ["loading", "btn-error"].map((x) => button.classList.toggle(x));
      }
    });
}