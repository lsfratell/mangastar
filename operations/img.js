const { aql } = require("arangojs");

NEWOPERATION("img", function ($) {
  FUNC.db()
    .query(
      aql`
        LET manga = FIRST((FOR m IN mangas FILTER m.slug == ${$.params.slug} RETURN m))
        FOR chapter IN chapters
          FILTER chapter.manga == manga.slug && chapter.slug == ${$.params.chapter}
        RETURN chapter`
    )
    .then((cursor) => cursor.all())
    .then((chapter) => {
      if (chapter.length > 0 && chapter[0].imgs[$.params.iid] !== undefined) {
        var builder = RESTBuilder.GET(chapter[0].imgs[$.params.iid]);
        builder.referrer("https://manganelo.com");
        builder.stream(function (err, response) {
          if (err) {
            $.invalid(err);
            return;
          }
          $.controller.stream(
            response.headers["content-type"],
            response.stream
          );
          $.cancel();
        });
      } else {
        $.controller.binary("", "image/jpeg");
        $.cancel();
      }
    });
});
