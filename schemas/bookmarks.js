const { aql } = require("arangojs");
const { flatMapDeep } = require("lodash");

NEWSCHEMA("Bookmark", function (schema) {
  schema.setQuery(function ($) {
    FUNC.db()
      .query(
        aql`
          FOR b IN bookmarks
            FILTER b.user_id == ${$.controller.user._id}
            LET mgs = (
              FOR m IN mangas
                SORT m.updated DESC
                FILTER m._id == b.manga_id
                LET lastChapter = FIRST((
                  FOR c IN chapters
                    SORT c.number DESC
                    FILTER c.manga == m.slug
                  RETURN { slug: c.slug, number: c.number }
                ))
                LET nextChapter = FIRST((
                  FOR c IN chapters
                    SORT c.number ASC
                    FILTER c.manga == m.slug
                    FILTER c.number > b.last_read
                  RETURN { slug: c.slug, number: c.number }
                ))
              RETURN MERGE(m, { lastChapter, nextChapter, lastRead: b.last_read })
            )
          RETURN mgs
        `
      )
      .then((cursor) => cursor.all())
      .then((bookmarks) => {
        $.callback({ bookmarks: flatMapDeep(bookmarks) });
      });
  });

  schema.setSave(function ($) {
    FUNC.db()
      .query(
        aql`
          INSERT {
            manga_id: ${"mangas/" + $.params.manga},
            user_id: ${$.controller.user._id},
            last_read: null,
          } INTO bookmarks RETURN NEW
        `
      )
      .then((cursor) => cursor.all())
      .then((bookmarks) => {
        $.controller.user.bookmarks.push(bookmarks[0]);
        $.success();
      })
      .catch((error) => {
        $.success(false, "Couldn't bookmark this manga.");
      });
  });

  schema.setRemove(function ($) {
    FUNC.db()
      .query(
        aql`
          FOR b IN bookmarks
            FILTER b.user_id == ${$.controller.user._id}
            FILTER b.manga_id == ${"mangas/" + $.params.manga}
          REMOVE b IN bookmarks RETURN OLD
        `
      )
      .then((cursor) => cursor.all())
      .then((deleted) => {
        $.controller.user.bookmarks = $.controller.user.bookmarks.filter(
          (i) => i._id !== deleted[0]._id
        );
        $.success();
      })
      .catch((error) => {
        $.success(false, "Couldn't bookmark this manga.");
      });
  });
});
