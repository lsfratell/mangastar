var { aql } = require("arangojs");

NEWSCHEMA("UserRegister", function (schema) {
  schema.define("username", "String", (val) => val.length >= 6);
  schema.define("password", "String", (val) => val.length >= 6);
  schema.define("displayname", "String", (val) => val.length >= 6);
  schema.define("email", "Email", true);

  schema.before("password", (value) => value.hash("sha1"));

  schema.setSave(function ($, model) {
    FUNC.db()
      .query(
        aql`
          INSERT {
            username: ${model.username},
            password: ${model.password},
            displayname: ${model.displayname},
            email: ${model.email},
            created: DATE_NOW()
          } INTO users RETURN NEW
        `
      )
      .then((cursor) => cursor.all())
      .then((user) => {
        $.success();
      }).catch(error => {
        if (error.message.includes("username")) {
          $.success(false, "Username already taken.");
        } else if (error.message.includes("displayname")) {
          $.success(false, "Display name already taken.");
        } else if (error.message.includes("email")) {
          $.success(false, "Email already taken.");
        } else {
          $.success(false, "Couln't finalize your request, try again later.");
        }
      });
  });
});

NEWSCHEMA("UserLogin", function (schema) {
  schema.define("username", "String", (val) => val.length >= 6);
  schema.define("password", "String", (val) => val.length >= 6);

  schema.before("password", (value) => value.hash("sha1"));

  schema.setSave(function ($, model) {
    FUNC.db()
      .query(
        aql`
          FOR u IN users
            FILTER u.username == ${model.username}
            FILTER u.password == ${model.password}
          RETURN u
        `
      )
      .then((cursor) => cursor.all())
      .then((user) => {
        if (user.length === 0) {
          $.success(false);
          return;
        }

        var opt = {};
        opt.name = CONF.auth_cookie;
        opt.key = CONF.auth_secret;
        opt.id = user[0]._id;
        opt.expire = '3 days';

        MAIN.session.setcookie($, opt, $.done());
      })
  });
});

NEWSCHEMA("UserLogout", function (schema) {
  schema.setSave(function ($) {
    MAIN.session.remove($.sessionid);
		$.cookie(CONF.cookie, '', '-10 year');
		$.redirect('/');
  });
});