var { aql } = require("arangojs");

NEWSCHEMA("Mangas", function (schema) {
  schema.addWorkflow("index", async function ($) {
    var cursor = await FUNC.db().query(
      aql`
        FOR m IN mangas
          SORT m.updated DESC
          LIMIT 0, 40
          LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC LIMIT 0, 3 RETURN c)
        RETURN MERGE(m, { chapters: chapters })
      `
    );

    var updates = await cursor.all();
    $.callback({ updates });
  });

  schema.addWorkflow("directory", async function ($) {
    var page = $.query.page ?? 1;
    var perPage = CONF.per_page;
    var title = "Directory";

    if ($.query.q) {
      title = "Search {0}".format($.query.q);
      var cursor = await FUNC.db().query(
        aql`
          FOR m IN FULLTEXT(mangas, 'search', ${$.query.q.split(" ").map((v, i) => i === 0 ? `prefix:${v}` : `|prefix:${v}`).join(",")})
            LIMIT ${(page - 1) * perPage}, ${perPage}
            SORT m.title ASC
            LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC LIMIT 0, 3 RETURN c)
          RETURN MERGE(m, { chapters: chapters })
        `,
        { count: true, fullCount: true }
      );
    } else if ($.query.status) {
      title = "{0} Mangas".format($.query.status.capitalize());
      var cursor = await FUNC.db().query(
        aql`
          FOR m IN mangas
            FILTER m.status.slug == ${$.query.status}
            LIMIT ${(page - 1) * perPage}, ${perPage}
            SORT m.title ASC
            LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC LIMIT 0, 3 RETURN c)
          RETURN MERGE(m, { chapters: chapters })
        `,
        { count: true, fullCount: true }
      );
    } else if ($.query.genre) {
      title = "{0} Mangas".format(DEF.genres.find(g => g.slug === $.query.genre).name);
      var cursor = await FUNC.db().query(
        aql`
          FOR m IN mangas
            FILTER ${$.query.genre} IN m.genres[*].slug
            LIMIT ${(page - 1) * perPage}, ${perPage}
            SORT m.title ASC
            LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC LIMIT 0, 3 RETURN c)
          RETURN MERGE(m, { chapters: chapters })
        `,
        { count: true, fullCount: true }
      );
    } else if ($.query.author) {
      title = "Mangas from author {0}".format(DEF.authors.find(g => g.slug === $.query.author).name);
      var cursor = await FUNC.db().query(
        aql`
          FOR m IN mangas
            FILTER ${$.query.author} IN m.authors[*].slug
            LIMIT ${(page - 1) * perPage}, ${perPage}
            SORT m.title ASC
            LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC LIMIT 0, 3 RETURN c)
          RETURN MERGE(m, { chapters: chapters })
        `,
        { count: true, fullCount: true }
      );
    } else if ($.query.order && $.query.order === "latest") {
      title = "Latest Mangas";
      var cursor = await FUNC.db().query(
        aql`
          FOR m IN mangas
            SORT m.updated DESC
            LIMIT ${(page - 1) * perPage}, ${perPage}
            LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC LIMIT 0, 3 RETURN c)
          RETURN MERGE(m, { chapters: chapters })
        `,
        { count: true, fullCount: true }
      );
    } else if ($.query.order && $.query.order === "new") {
      title = "New Mangas";
      var cursor = await FUNC.db().query(
        aql`
          FOR m IN mangas
            SORT m.created DESC
            LIMIT ${(page - 1) * perPage}, ${perPage}
            LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC LIMIT 0, 3 RETURN c)
          RETURN MERGE(m, { chapters: chapters })
        `,
        { count: true, fullCount: true }
      );
    } else {
      var cursor = await FUNC.db().query(
        aql`
          FOR m IN mangas
            SORT m.title ASC
            LIMIT ${(page - 1) * perPage}, ${perPage}
            LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC LIMIT 0, 3 RETURN c)
          RETURN MERGE(m, { chapters: chapters })
        `,
        { count: true, fullCount: true }
      );
    }

    var pagination = new Pagination(
      cursor.extra.stats.fullCount,
      page,
      perPage
    );

    var mangas = await cursor.all();
    $.callback({ mangas, pagination, q: $.query, title });
  });

  schema.addWorkflow("details", async function ($) {
    var cursor = await FUNC.db().query(
      aql`
        FOR m IN mangas
          FILTER m.slug == ${$.params.slug}
          LET chapters = (FOR c IN chapters FILTER c.manga == m.slug SORT c.number DESC RETURN c)
        RETURN MERGE(m, { chapters: chapters })
      `
    );

    var manga = (await cursor.all())[0];
    $.callback({ manga });
  });

  schema.addWorkflow("chapter", async function ($) {
    var cursor = await FUNC.db().query(
      aql`
        FOR m IN mangas
          FILTER m.slug == ${$.params.slug}
          FOR c IN chapters
            FILTER c.manga == m.slug
            FILTER c.slug == ${$.params.chapter}
            LIMIT 0, 1

          LET next = FIRST((
            FOR n IN chapters
              SORT n.number ASC
              FILTER n.manga == m.slug
              FILTER n.number > c.number
            RETURN n))

          LET prev = FIRST((
            FOR p IN chapters
              SORT p.number DESC
              FILTER p.manga == m.slug
              FILTER p.number < c.number
            RETURN p))

          LET mChapters = (
            FOR ch IN chapters
              SORT ch.number DESC
              FILTER ch.manga == m.slug
            RETURN ch
          )

        RETURN MERGE(m, { chapter: c, next, prev, chapters: mChapters })
      `
    );

    var manga = (await cursor.all())[0];

    if ($.controller.user) {
      const bookmarks = $.controller.user.bookmarks.map(x => x.manga_id);
      if (bookmarks.includes(manga._id)) {
        await FUNC.db().query(aql`
          FOR b IN bookmarks
            FILTER b.manga_id == ${manga._id}
            FILTER b.user_id == ${$.controller.user._id}
          UPDATE b WITH { last_read: ${manga.chapter.number} } IN bookmarks
        `)
      }
    }

    $.callback({ manga });
  });

  schema.addOperation("img", "img");
});
