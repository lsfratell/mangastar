exports.install = function () {
  ROUTE("+GET	  /bookmarks                 *Bookmark --> @query", 'bookmarks');
  ROUTE("+GET	  /bookmarks/add/{manga}     *Bookmark --> @save");
  ROUTE("+GET	  /bookmarks/remove/{manga}  *Bookmark --> @remove");
};