exports.install = function () {
  ROUTE("GET	/		                                                *Mangas --> @index",      "index");
  ROUTE("GET	/directory		                                      *Mangas --> @directory",  "directory");
  ROUTE("GET	/manga/{slug}		                                    *Mangas --> @details",    "details");
  ROUTE("GET	/manga/{slug}/read/{chapter}		                    *Mangas --> @chapter",    "chapter");
  ROUTE("GET	/manga/{slug}/read/{chapter}/img/{iid:number}		    *Mangas --> @img");
};