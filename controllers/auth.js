exports.install = function () {
  ROUTE("-POST	/register   *UserRegister   --> @save");
  ROUTE("-POST	/login      *UserLogin      --> @save");
  ROUTE("+GET	/logout       *UserLogout     --> @save");
};