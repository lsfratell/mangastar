# MangaStar
---
Website para ler mangas online, retira mangas automaticamente do site https://manganelo.com. Desktop e Mobile.

## Dependências
---
* NodeJS
* Python
* ArangoDB
* Gevent
* TotalJS

## Screenshots
---
* Desktop
	* [Home Page](https://bitbucket.org/lsfratell/mangastar/raw/210fd168510ce64e879b4cd8d06a2cbedce49896/screenshots/desktop_home_page.jpeg)
	* [Manga Page](https://bitbucket.org/lsfratell/mangastar/raw/210fd168510ce64e879b4cd8d06a2cbedce49896/screenshots/desktop_manga_description.jpeg)
	* [Ler Capítulo](https://bitbucket.org/lsfratell/mangastar/raw/210fd168510ce64e879b4cd8d06a2cbedce49896/screenshots/desktop_read_chapter.jpeg)
	* [Bookmarks Page](https://bitbucket.org/lsfratell/mangastar/raw/210fd168510ce64e879b4cd8d06a2cbedce49896/screenshots/desktop_bookmarks.jpeg)
* Mobile
	* [Home Page](https://bitbucket.org/lsfratell/mangastar/raw/210fd168510ce64e879b4cd8d06a2cbedce49896/screenshots/mobile_home_page.jpeg)
	* [Manga Page](https://bitbucket.org/lsfratell/mangastar/raw/210fd168510ce64e879b4cd8d06a2cbedce49896/screenshots/mobile_manga_description.jpeg)
	* [Ler Capítulo](https://bitbucket.org/lsfratell/mangastar/raw/210fd168510ce64e879b4cd8d06a2cbedce49896/screenshots/mobile_read_chapter.jpeg)
	* [Bookmarks Page](https://bitbucket.org/lsfratell/mangastar/raw/210fd168510ce64e879b4cd8d06a2cbedce49896/screenshots/mobile_bookmarks.jpeg)